<?php
declare(strict_types=1);
namespace MarsRovers\Decorators\Rover;

require_once "FinalPositionMessageInterface.php";

final class FinalPositionMessage implements FinalPositionMessageInterface
{

    public function byFinalPositionAndOrientationMessage(
        int $positionX,
        int $positionY,
        string $orientation
    ): string
    {
        return "The rover's final position is: " . $positionX . " " . $positionY . " " . $orientation;
    }
}
