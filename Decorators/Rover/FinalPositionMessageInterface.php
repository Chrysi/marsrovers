<?php
declare(strict_types=1);
namespace MarsRovers\Decorators\Rover;

interface FinalPositionMessageInterface
{
    public function byFinalPositionAndOrientationMessage(
        int $positionX,
        int $positionY,
        string $orientation
    ): string;
}
