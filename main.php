<?php

use MarsRovers\App;
use MarsRovers\Decorators\Rover\FinalPositionMessage;
use MarsRovers\Factories\Directions\Direction;
use MarsRovers\Repositories\Messages\Prompts;
use MarsRovers\Repositories\Movements\Movements;
use MarsRovers\Repositories\OrientationOptions\OrientationOptions;
use MarsRovers\Services\InputReader\ConsoleReader;
use MarsRovers\Services\MessagePrinter\ConsolePrinter;
use MarsRovers\Services\Movements\MoveForward;
use MarsRovers\Services\Rovers\PlateauData\PlateauDataReader;
use MarsRovers\Services\Rovers\Orientation\Reader as OrientationReader;
use MarsRovers\Services\Rovers\Commands\Reader as CommandsReader;
use MarsRovers\Services\Rovers\PlateauCoordinate\MaxCoordinateReader;
use MarsRovers\Services\Rovers\Position\Reader as PositionReader;
use MarsRovers\Services\Rovers\Movements\Rover;
use MarsRovers\Services\Rovers\Orientation\Validator as OrientationValidator;
use MarsRovers\Services\Rovers\Commands\Validator as CommandValidator;
use MarsRovers\Services\Rovers\PlateauCoordinate\Validator as CoordinateValidator;
use MarsRovers\Services\Rovers\Position\Validator as PositionValidator;
use MarsRovers\Services\Rovers\RoverData\RoverDataReader;
use MarsRovers\Services\Rovers\RoverData\Validator;

require_once "App.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\InputReader\ConsoleReader.php";
require_once "MarsRovers\Services\Rovers\PlateauCoordinate\Validator.php";
require_once "MarsRovers\Services\Rovers\Orientation\Validator.php";
require_once "MarsRovers\Services\Rovers\Commands\Validator.php";
require_once "MarsRovers\Services\Rovers\Position\Validator.php";
require_once "MarsRovers\Services\Rovers\Orientation\Reader.php";
require_once "MarsRovers\Repositories\OrientationOptions\OrientationOptions.php";
require_once "MarsRovers\Services\Rovers\Commands\Reader.php";
require_once "MarsRovers\Repositories\Movements\Movements.php";
require_once "MarsRovers\Consts\MessageKeys.php";
require_once "MarsRovers\Services\Rovers\PlateauCoordinate\MaxCoordinateReader.php";
require_once "MarsRovers\Services\Rovers\Position\Reader.php";
require_once "MarsRovers\Services\Rovers\Movements\Rover.php";
require_once "MarsRovers\Factories\Directions\Direction.php";
require_once "MarsRovers\Services\Movements\MoveForward.php";
require_once "MarsRovers\Decorators\Rover\FinalPositionMessage.php";
require_once "MarsRovers\Services\Rovers\PlateauData\PlateauDataReader.php";
require_once "MarsRovers\Services\Rovers\RoverData\Validator.php";

$prompts = new Prompts();
$consolePrinter = new ConsolePrinter();
$consoleReader = new ConsoleReader();
$orientations = new OrientationOptions();
$movements = new Movements();
$commands = new CommandsReader(
    $consoleReader,
    $consolePrinter,
    $prompts,
    new CommandValidator($movements)
);
$coordinates = new MaxCoordinateReader(
    $consoleReader,
    $consolePrinter,
    new CoordinateValidator()
);
$position = new PositionReader(
    $consoleReader,
    $consolePrinter,
    new PositionValidator(new Validator())
);
$orientation = new OrientationReader(
    $consoleReader,
    $consolePrinter,
    $prompts,
    new OrientationValidator($orientations)
);
$plateauData = new PlateauDataReader($prompts, $coordinates);
$roverData = new RoverDataReader($prompts,  $orientation, $position);
$rover = new Rover(
    $movements,
    new Direction($movements),
    new MoveForward($orientations)
);
$finalPositionMessage = new FinalPositionMessage();

$app = new App(
    $prompts,
    $consolePrinter,
    $roverData,
    $rover,
    $commands,
    $finalPositionMessage,
    $plateauData,
    new Validator()
);

try {
    $app->run();
} catch (OutOfBoundsException $exception) {
    die(Prompts::ERROR);
}


