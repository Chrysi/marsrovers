<?php
declare(strict_types=1);
namespace MarsRovers\Consts;

final class MessageKeys
{
    const WELCOME = 'welcome';
    const UPPER_RIGHT_COORDINATES = 'upper_right_coordinates';
    const LOWER_LEFT_COORDINATES = 'lower_left_coordinates';
    const ROVER_X_POSITION = 'rover_x_position';
    const ROVER_Y_POSITION = 'rover_y_direction';
    const ROVER_ORIENTATION = 'rover_orientation';
    const ROVER_COMMANDS = 'rover_path';
    const INVALID_INPUT = 'invalid_input';
    const INVALID_FINAL_POSITION = 'invalid_final_position';
}
