<?php
declare(strict_types=1);
namespace MarsRovers\Consts;

final class OrientationKeys
{
    const NORTH = 'north';
    const EAST = 'east';
    const SOUTH = 'south';
    const WEST = 'west';
}
