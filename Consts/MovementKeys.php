<?php
declare(strict_types=1);
namespace MarsRovers\Consts;

final class MovementKeys
{
    const TURN_LEFT = 'left';
    const TURN_RIGHT = 'right';
    const MOVE = 'move';
}
