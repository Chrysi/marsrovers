<?php
declare(strict_types=1);
namespace MarsRovers\Entities\Rovers;

final class RoverData
{
    private $x;
    private $y;
    private $orientation;

    public function __construct(int $x, int $y, string $orientation)
    {
        $this->x = $x;
        $this->y = $y;
        $this->orientation = $orientation;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getOrientation(): string
    {
        return $this->orientation;
    }
}