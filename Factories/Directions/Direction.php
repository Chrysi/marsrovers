<?php
declare(strict_types=1);
namespace MarsRovers\Factories\Directions;

use InvalidArgumentException;
use MarsRovers\Consts\MovementKeys;
use MarsRovers\Repositories\Movements\MovementsInterface;
use MarsRovers\Repositories\OrientationOptions\OrientationOptions;
use MarsRovers\Services\Movements\OrientationInterface;
use MarsRovers\Services\Movements\TurnLeftOrientation;
use MarsRovers\Services\Movements\TurnRightOrientation;
use OutOfBoundsException;

require_once "DirectionsInterface.php";
require_once "MarsRovers\Consts\MovementKeys.php";
require_once "MarsRovers\Repositories\Movements\Movements.php";
require_once "MarsRovers\Repositories\OrientationOptions\OrientationOptions.php";
require_once "MarsRovers\Services\Movements\TurnLeftOrientation.php";
require_once "MarsRovers\Services\Movements\TurnRightOrientation.php";

final class Direction implements DirectionsInterface
{
    private $movementsRepository;

    public function __construct(MovementsInterface $movementsRepository)
    {
        $this->movementsRepository = $movementsRepository;
    }

    /**
     * @throws InvalidArgumentException
     * @throws OutOfBoundsException
     */
    public function byCommand(string $direction): OrientationInterface
    {
        switch ($direction) {
            case $this->movementsRepository
                ->getMovement(MovementKeys::TURN_RIGHT):
                return new TurnRightOrientation(new OrientationOptions());
            case $this->movementsRepository
                ->getMovement(MovementKeys::TURN_LEFT):
                return new TurnLeftOrientation(new OrientationOptions());
            default:
                throw new InvalidArgumentException("Invalid direction: {$direction}");
        }
    }
}
