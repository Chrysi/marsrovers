<?php
declare(strict_types=1);
namespace MarsRovers\Factories\Directions;

use MarsRovers\Services\Movements\OrientationInterface;

interface DirectionsInterface
{
    public function byCommand(string $direction): OrientationInterface;
}
