<?php
declare(strict_types=1);
namespace MarsRovers;

use MarsRovers\Consts\MessageKeys;
use MarsRovers\Decorators\Rover\FinalPositionMessageInterface;
use MarsRovers\Repositories\Messages\MessageInterface;
use MarsRovers\Repositories\Messages\Prompts;
use MarsRovers\Services\MessagePrinter\PrinterInterface;
use MarsRovers\Services\Rovers\Commands\ReaderInterface;
use MarsRovers\Services\Rovers\Movements\RoverInterface;
use MarsRovers\Services\Rovers\PlateauData\PlateauDataReaderInterface;
use MarsRovers\Services\Rovers\RoverData\RoverDataInterfaceReader;
use MarsRovers\Services\Rovers\RoverData\Validator;
use OutOfBoundsException;

require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "MarsRovers\Consts\MessageKeys.php";
require_once "MarsRovers\Services\Rovers\RoverData\RoverDataReader.php";
require_once "MarsRovers\Decorators\Rover\FinalPositionMessage.php";
require_once "MarsRovers\Services\Rovers\Commands\Reader.php";
require_once "MarsRovers\Services\Rovers\Movements\Rover.php";
require_once "MarsRovers\Services\Rovers\PlateauData\PlateauDataReader.php";
require_once "MarsRovers\Services\Rovers\RoverData\Validator.php";

final class App
{
    private $prompts;
    private $consolePrinter;
    private $roverData;
    private $rover;
    private $commandsReader;
    private $finalPositionMessage;
    private $plateauData;
    private $finalPositionValidator;

    public function __construct(
        MessageInterface $prompts,
        PrinterInterface $consolePrinter,
        RoverDataInterfaceReader $roverData,
        RoverInterface $rover,
        ReaderInterface $commandsReader,
        FinalPositionMessageInterface $finalPositionMessage,
        PlateauDataReaderInterface $plateauData,
        Validator $finalPositionValidator
    ) {

        $this->prompts = $prompts;
        $this->consolePrinter = $consolePrinter;
        $this->roverData = $roverData;
        $this->rover = $rover;
        $this->commandsReader = $commandsReader;
        $this->finalPositionMessage = $finalPositionMessage;
        $this->plateauData = $plateauData;
        $this->finalPositionValidator = $finalPositionValidator;
    }

    /** @throws OutOfBoundsException */
    public function run()
    {
        $welcomePrompt = $this->prompts->getMessage(MessageKeys::WELCOME);
        $this->consolePrinter->printMessage($welcomePrompt);

        $coordinatesData = $this->plateauData->getData();
        $maxX = $coordinatesData->getMaxX();
        $maxY = $coordinatesData->getMaxY();
        $finalData = [];

        for ($i = 0; $i < 2; $i++) {

            try {
                $data = $this->roverData->getData($maxX, $maxY);
            } catch (OutOfBoundsException $exception) {
                die(Prompts::ERROR);
            }

            try {
                $commands = $this->commandsReader->getCommands();
            } catch (OutOfBoundsException $exception) {
                die(Prompts::ERROR);
            }

            try {
                $finalData[] = $this->rover->act($data, $commands);
            } catch (OutOfBoundsException $exception) {
                die(Prompts::ERROR);
            }
        }

        $invalidFinalPositionPrompt = $this->prompts->getMessage(MessageKeys::INVALID_FINAL_POSITION);
        foreach ($finalData as $roverData) {
            $finalMessage = $this->finalPositionMessage
                ->byFinalPositionAndOrientationMessage(
                    $roverData->getX(),
                    $roverData->getY(),
                    $roverData->getOrientation()
                );
            $this->consolePrinter->printMessage($finalMessage);
            if (!$this->finalPositionValidator->isPositionValid($roverData->getX(), $maxX)) {
                $this->consolePrinter->printMessage($invalidFinalPositionPrompt);
            }
            if (!$this->finalPositionValidator->isPositionValid($roverData->getY(), $maxY)) {
                $this->consolePrinter->printMessage($invalidFinalPositionPrompt);
            }
        }
    }
}
