<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\Movements;

interface MovementsInterface
{
    public function getMovement(string $key): string;

    /** @returns string */
    public function getMovements(): array;
}
