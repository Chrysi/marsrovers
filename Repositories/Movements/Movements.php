<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\Movements;

use MarsRovers\Consts\MovementKeys;
use OutOfBoundsException;

require_once "MovementsInterface.php";
require_once "MarsRovers\Consts\MovementKeys.php";

final class Movements implements MovementsInterface
{

    private $movements = [
        MovementKeys::TURN_LEFT => 'L',
        MovementKeys::TURN_RIGHT => 'R',
        MovementKeys::MOVE => 'M',
    ];

    /** @throws OutOfBoundsException */
    public function getMovement(string $key): string
    {
        if (!array_key_exists($key, $this->movements)) {
            throw new OutOfBoundsException($key ." does not exist.");
        }
        return $this->movements[$key];
    }

    public function getMovements(): array
    {
        return $this->movements;
    }
}
