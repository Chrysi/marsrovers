<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\OrientationOptions;

use MarsRovers\Consts\OrientationKeys;

require_once "OrientationOptionsInterface.php";

final class OrientationOptions implements OrientationOptionsInterface
{

    private $orientation = [
        OrientationKeys::NORTH => 'N',
        OrientationKeys::EAST => 'E',
        OrientationKeys::SOUTH => 'S',
        OrientationKeys::WEST => 'W',
    ];

    public function getOrientation(string $key): string
    {
        return $this->orientation[$key];
    }

    public function getOrientations(): array
    {
        return $this->orientation;
    }
}
