<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\OrientationOptions;

interface OrientationOptionsInterface
{
    public function getOrientation(string $key): string;

    /** @returns string */
    public function getOrientations(): array;
}
