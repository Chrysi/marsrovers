<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\Messages;

use MarsRovers\Consts\MessageKeys;
use OutOfBoundsException;

require_once "MessageInterface.php";

final class Prompts implements MessageInterface
{

    const ERROR = "An error has occured.";
    private $messages = [
        MessageKeys::WELCOME => 'Welcome to Mars! You are currently standing at a rectangular plateau.',
        MessageKeys::UPPER_RIGHT_COORDINATES => 'Enter maxX: ',
        MessageKeys::LOWER_LEFT_COORDINATES => 'Enter maxY: ',
        MessageKeys::ROVER_X_POSITION => 'Give the x position of the rover: ',
        MessageKeys::ROVER_Y_POSITION => 'Give the y position of the rover: ',
        MessageKeys::ROVER_ORIENTATION => 'Give the orientation of the rover: ',
        MessageKeys::ROVER_COMMANDS => 'Give the commands that the rover will follow: ',
        MessageKeys::INVALID_INPUT => 'Invalid input.',
        MessageKeys::INVALID_FINAL_POSITION => 'Invalid input. The rover is not inside the plateau.'

    ];

    /** @throws OutOfBoundsException */
    public function getMessage(string $key): string
    {
        if (!array_key_exists($key, $this->messages)) {
            throw new OutOfBoundsException($key ." does not exist.");
        }
        return $this->messages[$key];
    }
}
