<?php
declare(strict_types=1);
namespace MarsRovers\Repositories\Messages;

interface MessageInterface
{
    public function getMessage(string $key): string;
}
