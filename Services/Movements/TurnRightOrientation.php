<?php
declare(strict_types=1);
namespace MarsRovers\Services\Movements;

use InvalidArgumentException;
use MarsRovers\Consts\OrientationKeys;
use MarsRovers\Repositories\OrientationOptions\OrientationOptionsInterface;
use OutOfBoundsException;

require_once "OrientationInterface.php";
require_once "Marsrovers\Consts\OrientationKeys.php";
require_once "MarsRovers\Repositories\OrientationOptions\OrientationOptions.php";

final class TurnRightOrientation implements OrientationInterface
{
    private $orientationsRepository;

    public function __construct(OrientationOptionsInterface $orientationsRepository)
    {
        $this->orientationsRepository = $orientationsRepository;
    }

    /**
     * @throws OutOfBoundsException
     * *@throws InvalidArgumentException
     */
    public function getOrientation(string $orientation): string
    {
        switch ($orientation) {
            case $this->orientationsRepository->getOrientation(OrientationKeys::NORTH):
                return $this->orientationsRepository->getOrientation(OrientationKeys::EAST);
            case $this->orientationsRepository->getOrientation(OrientationKeys::EAST):
                return $this->orientationsRepository->getOrientation(OrientationKeys::SOUTH);
            case $this->orientationsRepository->getOrientation(OrientationKeys::SOUTH):
                return $this->orientationsRepository->getOrientation(OrientationKeys::WEST);
            case $this->orientationsRepository->getOrientation(OrientationKeys::WEST):
                return $this->orientationsRepository->getOrientation(OrientationKeys::NORTH);
            default:
                throw new InvalidArgumentException("Invalid orientation: {$orientation}");
        }
    }
}
