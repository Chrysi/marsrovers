<?php
declare(strict_types=1);
namespace MarsRovers\Services\Movements;

use InvalidArgumentException;
use MarsRovers\Consts\OrientationKeys;
use MarsRovers\Entities\Rovers\CoordinatesData;
use MarsRovers\Repositories\OrientationOptions\OrientationOptionsInterface;
use OutOfBoundsException;


require_once "Marsrovers\Consts\OrientationKeys.php";
require_once "MovementInterface.php";
require_once "MarsRovers\Repositories\OrientationOptions\OrientationOptions.php";
require_once "MarsRovers\Entities\Rovers\CoordinatesData.php";


final class MoveForward implements MovementInterface
{
    private $orientationsRepository;

    public function __construct(OrientationOptionsInterface $orientationsRepository)
    {
        $this->orientationsRepository = $orientationsRepository;
    }

    /**
     * @throws OutOfBoundsException
     * @throws InvalidArgumentException
     */
    public function getPosition(int $positionX, int $positionY, string $orientation): CoordinatesData
    {
        switch ($orientation) {
            case $this->orientationsRepository->getOrientation(OrientationKeys::NORTH):
                return new CoordinatesData($positionX, $positionY + 1);
            case $this->orientationsRepository->getOrientation(OrientationKeys::EAST):
                return new CoordinatesData($positionX + 1, $positionY);
            case $this->orientationsRepository->getOrientation(OrientationKeys::SOUTH):
                return new CoordinatesData($positionX, $positionY - 1);
            case $this->orientationsRepository->getOrientation(OrientationKeys::WEST):
                return new CoordinatesData($positionX - 1, $positionY);
            default:
                throw new InvalidArgumentException("Invalid orientation: {$orientation}");
        }
    }
}
