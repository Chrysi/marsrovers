<?php
declare(strict_types=1);
namespace MarsRovers\Services\Movements;

interface OrientationInterface
{
    public function getOrientation(string $orientation): string;
}
