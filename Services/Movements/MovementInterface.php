<?php
declare(strict_types=1);
namespace MarsRovers\Services\Movements;

use MarsRovers\Entities\Rovers\CoordinatesData;

interface MovementInterface
{
    public function getPosition(int $positionX, int $positionY, string $orientation): CoordinatesData;
}
