<?php
declare(strict_types=1);
namespace MarsRovers\Services\InputReader;

require_once "InputReaderInterface.php";

final class ConsoleReader implements InputReaderInterface
{

    public function getInput(string $prompt): string
    {
        return readline($prompt);
    }
}
