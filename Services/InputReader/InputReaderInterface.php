<?php
declare(strict_types=1);
namespace MarsRovers\Services\InputReader;

interface InputReaderInterface
{
    public function getInput(string $prompt): string;
}
