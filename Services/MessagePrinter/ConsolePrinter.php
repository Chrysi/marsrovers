<?php
declare(strict_types=1);
namespace MarsRovers\Services\MessagePrinter;
require_once "PrinterInterface.php";

final class ConsolePrinter implements PrinterInterface
{

    public function printMessage(string $message)
    {
        echo $message . PHP_EOL;
    }
}
