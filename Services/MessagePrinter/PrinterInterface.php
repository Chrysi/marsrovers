<?php
declare(strict_types=1);
namespace MarsRovers\Services\MessagePrinter;

interface PrinterInterface
{
    public function printMessage(string $message);
}
