<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Movements;

use InvalidArgumentException;
use MarsRovers\Consts\MovementKeys;
use MarsRovers\Entities\Rovers\RoverData;
use MarsRovers\Factories\Directions\DirectionsInterface;
use MarsRovers\Repositories\Movements\MovementsInterface;
use MarsRovers\Services\Movements\MovementInterface;
use OutOfBoundsException;

require_once "RoverInterface.php";
require_once "MarsRovers\Consts\MovementKeys.php";
require_once "MarsRovers\Entities\Rovers\RoverData.php";
require_once "MarsRovers\Repositories\Movements\Movements.php";
require_once "MarsRovers\Factories\Directions\Direction.php";
require_once "MarsRovers\Services\Movements\MoveForward.php";

final class Rover implements RoverInterface
{
    private $movementsRepository;
    private $directions;
    private $moveForward;

    public function __construct(
        MovementsInterface $movementsRepository,
        DirectionsInterface $directions,
        MovementInterface $moveForward)
    {
        $this->movementsRepository = $movementsRepository;
        $this->directions = $directions;
        $this->moveForward = $moveForward;
    }

    /**
     * @throws InvalidArgumentException
     * @throws OutOfBoundsException
     */
    public function act(RoverData $roverData, string $commands): RoverData
    {

        $orientation = $roverData->getOrientation();
        $x = $roverData->getX();
        $y = $roverData->getY();
        foreach (str_split($commands) as $command) {
            switch ($command) {
                case $this->movementsRepository->getMovement(MovementKeys::TURN_RIGHT):
                case $this->movementsRepository->getMovement(MovementKeys::TURN_LEFT):
                     $orientation = $this->directions
                         ->byCommand($command)
                         ->getOrientation($orientation);
                     break;
                case $this->movementsRepository->getMovement(MovementKeys::MOVE):
                    $newPosition = $this->moveForward->getPosition($x, $y, $orientation);
                    $x = $newPosition->getX();
                    $y = $newPosition->getY();
                    break;
                default:
                    throw new InvalidArgumentException("Invalid command: {$command}");
            }
        }
        return new RoverData($x, $y, $orientation);
    }
}
