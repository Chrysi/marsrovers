<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Movements;

use MarsRovers\Entities\Rovers\RoverData;

require_once "MarsRovers\Entities\Rovers\RoverData.php";

interface RoverInterface
{
    public function act(RoverData $roverData, string $commands): RoverData;
}
