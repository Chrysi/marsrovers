<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Orientation;

interface ReaderInterface
{
    public function getOrientation(): string;
}
