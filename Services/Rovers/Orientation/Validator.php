<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Orientation;

use MarsRovers\Repositories\OrientationOptions\OrientationOptionsInterface;

require_once "ValidatorInterface.php";
require_once "MarsRovers\Repositories\OrientationOptions\OrientationOptions.php";

 final class Validator implements ValidatorInterface
 {
     private $orientationsRepository;

     public function __construct(OrientationOptionsInterface $orientationsRepository)
     {

         $this->orientationsRepository = $orientationsRepository;
     }

     public function isOrientationValid(string $orientation): bool
     {
         $orientationRepository = $this->orientationsRepository->getOrientations();
         return in_array($orientation, $orientationRepository);
     }
 }
