<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Orientation;

use MarsRovers\Consts\MessageKeys;
use MarsRovers\Repositories\Messages\MessageInterface;
use MarsRovers\Services\InputReader\InputReaderInterface;
use MarsRovers\Services\MessagePrinter\PrinterInterface;
use OutOfBoundsException;

require_once "ReaderInterface.php";
require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\InputReader\ConsoleReader.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "Validator.php";

final class Reader implements ReaderInterface
{
    private $reader;
    private $printer;
    private $prompts;
    private $orientationValidator;

    public function __construct(
        InputReaderInterface $reader,
        PrinterInterface $printer,
        MessageInterface $prompts,
        ValidatorInterface $orientationValidator
    ) {
        $this->reader = $reader;
        $this->printer = $printer;
        $this->prompts = $prompts;
        $this->orientationValidator = $orientationValidator;
    }

    /** @throws OutOfBoundsException */
    public function getOrientation(): string
    {
        $orientationPrompt = $this->prompts->getMessage(MessageKeys::ROVER_ORIENTATION);
        $invalidInput = $this->prompts->getMessage(MessageKeys::INVALID_INPUT);
        $userInput = $this->reader->getInput($orientationPrompt);
        while (!$this->orientationValidator->isOrientationValid($userInput)) {
            $this->printer->printMessage($invalidInput);
            $userInput = $this->reader->getInput($orientationPrompt);
        }
        return $userInput;
    }
}
