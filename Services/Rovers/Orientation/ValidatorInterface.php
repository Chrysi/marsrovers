<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Orientation;

interface ValidatorInterface
{
    public function isOrientationValid(string $orientation): bool;
}
