<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauCoordinate;

use InvalidArgumentException;
use MarsRovers\Services\InputReader\InputReaderInterface;
use MarsRovers\Services\MessagePrinter\PrinterInterface;

require_once "ReaderInterface.php";
require_once "MarsRovers\Services\InputReader\ConsoleReader.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "Validator.php";

final class MaxCoordinateReader implements ReaderInterface
{

    private $reader;
    private $printer;
    private $coordinateValidator;

    public function __construct(
        InputReaderInterface $reader,
        PrinterInterface $printer,
        ValidatorInterface $coordinateValidator
    ) {

        $this->reader = $reader;
        $this->printer = $printer;
        $this->coordinateValidator = $coordinateValidator;
    }

    /** @throws InvalidArgumentException */
    public function getCoordinate(string $coordinatePromptKey, string $invalidPromptKey): int
    {
        $userInput = $this->reader->getInput($coordinatePromptKey);
        while (!$this->coordinateValidator->isCoordinateValid($userInput)) {
            $this->printer->printMessage($invalidPromptKey);
            $userInput = $this->reader->getInput($coordinatePromptKey);
        }
        return intval($userInput);
    }

}
