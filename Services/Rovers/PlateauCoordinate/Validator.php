<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauCoordinate;

require_once "ValidatorInterface.php";

final class Validator implements ValidatorInterface
{

    public function isCoordinateValid(string $coordinate): bool
    {
        if (!is_numeric($coordinate)) {
            return false;
        }
        $coordinateNumber = intval($coordinate);
        return ($coordinateNumber > 0);
    }
}
