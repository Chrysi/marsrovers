<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauCoordinate;

interface ReaderInterface
{
    public function getCoordinate(string $coordinatePromptKey, string $invalidPromptKey): int;
}
