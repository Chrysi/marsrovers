<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauCoordinate;

interface ValidatorInterface
{
    public function isCoordinateValid(string $coordinate): bool;
}
