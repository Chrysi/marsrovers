<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\RoverData;

require_once "ValidatorInterface.php";

final class Validator implements ValidatorInterface
{

    public function isPositionValid(int $position, int $coordinate): bool
    {
        return ($position >= 0 && $position <= $coordinate);
    }
}
