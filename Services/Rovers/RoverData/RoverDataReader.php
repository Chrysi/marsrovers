<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\RoverData;

use MarsRovers\Entities\Rovers\RoverData as Data;
use MarsRovers\Consts\MessageKeys;
use MarsRovers\Repositories\Messages\MessageInterface;
use MarsRovers\Repositories\Messages\Prompts;
use MarsRovers\Services\Rovers\Orientation\ReaderInterface as OrientationReaderInterface;
use MarsRovers\Services\Rovers\Position\ReaderInterface as PositionReaderInterface;
use OutOfBoundsException;

require_once "MarsRovers\Entities\Rovers\RoverData.php";
require_once "RoverDataInterfaceReader.php";
require_once "MarsRovers\Consts\MessageKeys.php";
require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\Rovers\Orientation\Reader.php";
require_once "MarsRovers\Services\Rovers\Position\Reader.php";

final class RoverDataReader implements RoverDataInterfaceReader
{

    private $prompts;
    private $orientationReader;
    private $positionReader;

    public function __construct(
        MessageInterface $prompts,
        OrientationReaderInterface $orientationReader,
        PositionReaderInterface $positionReader
    ) {
        $this->prompts = $prompts;
        $this->orientationReader = $orientationReader;
        $this->positionReader = $positionReader;
    }

    /** @throws OutOfBoundsException */
    public function getData(int $maxX, int $maxY): Data
    {

        $positionX = $this->positionReader->getPosition(
            $this->prompts->getMessage(MessageKeys::ROVER_X_POSITION),
            $this->prompts->getMessage(MessageKeys::INVALID_INPUT),
            $maxX
        );

        $positionY = $this->positionReader->getPosition(
            $this->prompts->getMessage(MessageKeys::ROVER_Y_POSITION),
            $this->prompts->getMessage(MessageKeys::INVALID_INPUT),
            $maxY
        );

        try {
            $orientation = $this->orientationReader->getOrientation();
        } catch (OutOfBoundsException $exception) {
            die(Prompts::ERROR);
        }

        return new Data($positionX, $positionY, $orientation);
    }
}



