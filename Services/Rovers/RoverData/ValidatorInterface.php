<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\RoverData;

interface ValidatorInterface
{
    public function isPositionValid(int $position, int $coordinate): bool;
}