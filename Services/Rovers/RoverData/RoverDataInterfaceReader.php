<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\RoverData;

use MarsRovers\Entities\Rovers\RoverData;

require_once "MarsRovers\Entities\Rovers\RoverData.php";

interface RoverDataInterfaceReader
{
    public function getData(int $maxX, int $maxY): RoverData;
}