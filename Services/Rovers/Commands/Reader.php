<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Commands;

use MarsRovers\Consts\MessageKeys;
use MarsRovers\Repositories\Messages\MessageInterface;
use MarsRovers\Services\InputReader\InputReaderInterface;
use MarsRovers\Services\MessagePrinter\PrinterInterface;
use OutOfBoundsException;

require_once "ReaderInterface.php";
require_once "MarsRovers\Consts\MessageKeys.php";
require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\InputReader\ConsoleReader.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "Validator.php";

final class Reader implements ReaderInterface
{
    private $reader;
    private $printer;
    private $prompt;
    private $commandValidator;

    public function __construct(
        InputReaderInterface $reader,
        PrinterInterface $printer,
        MessageInterface $prompt,
        ValidatorInterface $commandValidator
    ) {
        $this->reader = $reader;
        $this->printer = $printer;
        $this->prompt = $prompt;
        $this->commandValidator = $commandValidator;
    }

    /** @throws OutOfBoundsException */
    public function getCommands(): string
    {
        $promptCommands = $this->prompt->getMessage(MessageKeys::ROVER_COMMANDS);
        $invalidInput = $this->prompt->getMessage(MessageKeys::INVALID_INPUT);
        $userInput = $this->reader->getInput($promptCommands);
        while (!$this->commandValidator->areCommandsValid($userInput)) {
            $this->printer->printMessage($invalidInput);
            $userInput = $this->reader->getInput($promptCommands);
        }
        return $userInput;
    }
}
