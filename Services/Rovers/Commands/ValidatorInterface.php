<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Commands;

interface ValidatorInterface
{
    public function areCommandsValid(string $commands): bool;
}
