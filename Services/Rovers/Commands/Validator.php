<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Commands;

use MarsRovers\Repositories\Movements\MovementsInterface;

require_once "ValidatorInterface.php";
require_once "MarsRovers\Repositories\Movements\Movements.php";

final class Validator implements ValidatorInterface
{
    private $movementsRepository;

    public function __construct(MovementsInterface $movementsRepository)
    {

        $this->movementsRepository = $movementsRepository;
    }

    public function areCommandsValid(string $commands): bool
    {
        $allMovements = $this->movementsRepository->getMovements();
        $commandsChars = str_split($commands);
        foreach ($commandsChars as $commandChar) {
            if (!in_array($commandChar, $allMovements)) {
                return false;
            }
        }
        return true;
    }
}
