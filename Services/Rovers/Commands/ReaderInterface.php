<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Commands;

interface ReaderInterface
{
    public function getCommands(): string;
}
