<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Position;

interface ReaderInterface
{
    public function getPosition(string $positionPromptKey, string $invalidPromptKey, int $coordinate): int;
}