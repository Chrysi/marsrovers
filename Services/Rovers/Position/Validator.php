<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Position;

require_once "ValidatorInterface.php";

final class Validator implements ValidatorInterface
{
    public function isPositionValid(string $position, string $coordinate): bool
    {
        if (!is_numeric($position)) {
            return false;
        }

        $positionNumber = intval($position);
        $coordinateNumber = intval($coordinate);
        return ($positionNumber >= 0 && $positionNumber <= $coordinateNumber);
    }
}
