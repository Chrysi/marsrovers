<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Position;

interface ValidatorInterface
{
    public function isPositionValid(string $position, string $coordinate): bool;
}
