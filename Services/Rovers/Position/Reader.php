<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\Position;

use MarsRovers\Services\InputReader\InputReaderInterface;
use MarsRovers\Services\MessagePrinter\PrinterInterface;


require_once "ReaderInterface.php";
require_once "MarsRovers\Services\InputReader\ConsoleReader.php";
require_once "MarsRovers\Services\MessagePrinter\ConsolePrinter.php";
require_once "Validator.php";

final class Reader implements ReaderInterface
{

    private $reader;
    private $printer;
    private $positionValidator;

    public function __construct(
        InputReaderInterface $reader,
        PrinterInterface $printer ,
        ValidatorInterface $positionValidator
    ) {
        $this->reader = $reader;
        $this->printer = $printer;
        $this->positionValidator = $positionValidator;
    }

    public function getPosition(string $positionPromptKey, string $invalidPromptKey, int $coordinate): int
    {
        $userInput = $this->reader->getInput($positionPromptKey);
        while (!$this->positionValidator->isPositionValid($userInput, strval($coordinate))) {
            $this->printer->printMessage($invalidPromptKey);
            $userInput = $this->reader->getInput($positionPromptKey);
        }
        return intval($userInput);
    }
}
