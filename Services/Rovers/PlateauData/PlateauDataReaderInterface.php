<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauData;

use MarsRovers\Entities\Rovers\PlateauData;

interface PlateauDataReaderInterface
{
    public function getData(): PlateauData;
}
