<?php
declare(strict_types=1);
namespace MarsRovers\Services\Rovers\PlateauData;

use MarsRovers\Consts\MessageKeys;
use MarsRovers\Entities\Rovers\PlateauData as Data;
use MarsRovers\Repositories\Messages\MessageInterface;
use MarsRovers\Services\Rovers\PlateauCoordinate\ReaderInterface;
use OutOfBoundsException;

require_once "PlateauDataReaderInterface.php";
require_once "MarsRovers\Consts\MessageKeys.php";
require_once "MarsRovers\Entities\Rovers\PlateauData.php";
require_once "MarsRovers\Repositories\Messages\Prompts.php";
require_once "MarsRovers\Services\Rovers\PlateauCoordinate\MaxCoordinateReader.php";

final class PlateauDataReader implements PlateauDataReaderInterface
{
    private $prompts;
    private $coordinateReader;

    public function __construct(

        MessageInterface $prompts,
        ReaderInterface $coordinateReader)
    {

        $this->prompts = $prompts;
        $this->coordinateReader = $coordinateReader;
    }

    /** @throws OutOfBoundsException */
    public function getData(): Data
    {
        $maxX = $this->coordinateReader->getCoordinate(
            $this->prompts->getMessage(MessageKeys::UPPER_RIGHT_COORDINATES),
            $this->prompts->getMessage(MessageKeys::INVALID_INPUT)
        );

        $maxY = $this->coordinateReader->getCoordinate(
            $this->prompts->getMessage(MessageKeys::LOWER_LEFT_COORDINATES),
            $this->prompts->getMessage(MessageKeys::INVALID_INPUT)
        );
        return new Data($maxX, $maxY);
    }
}